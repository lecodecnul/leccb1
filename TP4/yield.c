#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include "hardware.h"
#include "hwconfig.h"

#define CTXMAGIC 0xCAFEBABE
#define WORDSIZE 16 
#define LOOP 5000
#define TIME_TIMER 1

enum ctx_state_e { CTX_INIT, CTX_EXEC, CTX_END };

typedef void (func_t)(void *);
typedef void (* func_irq_t) ();
void start();

struct ctx_s {
	unsigned int ctx_magicbase;
	void * ctx_rsp;
	void * ctx_rbp;
	unsigned * ctx_base;
	func_t * ctx_f;
	void * ctx_arg;
	enum ctx_state_e ctx_state;
	struct ctx_s *ctx_next; 
	struct ctx_s *ctx_prev; 
};

struct ctx_s * ring = NULL;
struct ctx_s * current_ctx = NULL;

void set_timer(int ms)
{
	_out(TIMER_PARAM,128+64); 
	_out(TIMER_ALARM,0xFFFFFFFF - ms);
}

void irq_disable()
{
	_mask(15);
}

void irq_enable()
{
	set_timer(TIME_TIMER);
	_mask(1);
}

void create_ctx(int stack_size, func_t * f, void * arg)
{
	/* On crée dynamiquement le prochain contexte */
	struct ctx_s * new;
	new = malloc(sizeof(struct ctx_s));
	assert(new);


	/* Initalisation du nouveau contexte */
	new->ctx_magicbase = CTXMAGIC;	
	new->ctx_base = malloc(stack_size);
	assert(new->ctx_base);

	new->ctx_rsp = new->ctx_rbp = new->ctx_base + stack_size - WORDSIZE;
	new->ctx_f = f; 
	new->ctx_arg = arg; 
	new->ctx_state = CTX_INIT; 

	/* Cas où le début de chaine n'a pas déja été initialisé */
	if (ring == NULL){
		/* On l'initialise avec le context qu'on vient de créer */
		ring = new;	
		new->ctx_prev = new;
	}else {
		current_ctx->ctx_next = new;
		ring->ctx_prev = new;
		new->ctx_prev = current_ctx;
	}
	new->ctx_next = ring;
	current_ctx = new;
}

int switch_to(struct ctx_s * newctx)
{
	assert(newctx->ctx_magicbase == CTXMAGIC);	

	irq_disable();
	/* Tant que l'on tombe sur des contextes terminés */
	while(newctx->ctx_state==CTX_END){
		if(newctx->ctx_next == newctx){
			/* Les contextes sont tous terminés, on est à la fin de la chaine */
			free(newctx->ctx_base);
			free(newctx);
			
			exit(EXIT_SUCCESS);
		}else {
			/* On libère la pile qui ne sert plus */
			free(newctx->ctx_base);

			/* On met à jour la chaine de contexte */
			newctx->ctx_prev->ctx_next = newctx->ctx_next;
			newctx->ctx_next->ctx_prev= newctx->ctx_prev;

			/* On libère le context terminé */
			free(newctx);

			newctx = current_ctx->ctx_next;
		}
	}

	if (current_ctx) {
		asm("mov %%esp, %0" : "=r"(current_ctx->ctx_rsp));
		asm("mov %%ebp, %0" : "=r"(current_ctx->ctx_rbp)); 
	}

	current_ctx = newctx;

	asm("mov %0, %%esp"::"r" (current_ctx->ctx_rsp));
	asm("mov %0, %%ebp"::"r" (current_ctx->ctx_rbp));
	irq_enable();

	if(current_ctx->ctx_state == CTX_INIT){
		start();
	}
	
	return 0;
}

void yield()
{
	if(current_ctx){
		switch_to(current_ctx->ctx_next);
	}
}

void start()
{
	current_ctx->ctx_state = CTX_EXEC;
	current_ctx->ctx_f(current_ctx->ctx_arg);
	current_ctx->ctx_state = CTX_END;
	yield();
}


void f_ping(void *args)
{
    for(int i=0; i<LOOP; i++) {
        printf("A") ;
        printf("B") ;
        printf("C") ;
    }
}

void f_pong(void *args)
{
    for(int i=0; i<LOOP; i++) {
        printf("1") ;
        printf("2") ;
    }
} 

void f_pang(void *args)
{
    for(int i=0; i<LOOP; i++) {
        printf("{") ;
        printf("}") ;
    }
} 

void empty_it()
{
    return;
}

void start_schedule()
{
	if (init_hardware(INIFILENAME) == 0) {
		fprintf(stderr, "Error in hardware initialization\n");
		exit(EXIT_FAILURE);
	}

	for (int i=0; i<16; i++)
		IRQVECTOR[i] = empty_it;

	IRQVECTOR[TIMER_IRQ] = yield;    
	irq_enable();
}

int main()
{
	unsigned int i;

	create_ctx(16384, f_ping, NULL);
	create_ctx(16384, f_pong, NULL);
	create_ctx(16384, f_pang, NULL);
	
	start_schedule();
	
	/* On atteeeeeeeeend */
	for (i=0; i<(1<<28); i++)
		;

	return 0;
}

